@extends('frontend.common.template')

@section('content')

    <div class="main servicos servicos-{{ $slug }}">
        <div class="center">
            <h2>Serviços</h2>
            <h3>{{ $paginas[$slug] }}</h3>

            <div class="texto-1">
                {!! $servicos->recrutamento_texto1 !!}
                <div class="etapas">
                    <h4>A metodologia de trabalho da Potenza é composta de 7 etapas:</h4>
                    {!! $servicos->recrutamento_etapas !!}
                </div>
            </div>
            <div class="texto-2">
                <img src="{{ asset('assets/img/layout/infografico-texto.png') }}" alt="">
                {!! $servicos->recrutamento_texto2 !!}
            </div>
        </div>
    </div>

@endsection