(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.mobileSubToggle = function() {
        var $handle = $('.sub-mobile-handle');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $('#nav-mobile').find('.sub-nav').slideToggle();
            $handle.parent().parent().toggleClass('open');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.home-banners');
        if (!$wrapper) return;

        $wrapper.cycle({
            slides: '>.banner'
        });
    };

    App.bannerResizeText = function() {
        $('.home-banners').find('span').each(function() {
            var _this = $(this),
                windowWidth = $(window).width();

            if(windowWidth >= 1180) {
                _this.css('font-size', _this.data('desktop-size'));
            } else {
                _this.css('font-size', function() {
                    var proportion = ((windowWidth*100) / 1300) / 100,
                        fontsize   = parseInt(_this.data('desktop-size')),
                        size       = fontsize * proportion;

                    return (size >= 16 ? size : 16);
                });
            }

            _this.fadeIn();
        });
    };

    App.terceirizacaoCycle = function() {
        var $wrapper = $('.terceirizacao-cycle');
        if (!$wrapper.length) return;

        $wrapper.find('.slide').css({visibility:'hidden'});

        $wrapper.cycle({
            slides: '>.slide',
            timeout: 0,
            autoHeight: 'calc'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.mobileSubToggle();
        this.bannersHome();
        this.bannerResizeText();
        $(window).resize(this.bannerResizeText);
        this.terceirizacaoCycle();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
