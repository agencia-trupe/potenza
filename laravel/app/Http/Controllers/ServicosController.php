<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index($slug = 'nossos-servicos')
    {
        $servicos = Servicos::first();
        $paginas  = Servicos::$paginas;

        if (!array_key_exists($slug, $paginas)) \App::abort('404');

        return view('frontend.servicos.' . $slug, compact('servicos', 'paginas', 'slug'));
    }
}
