@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h2>Contato</h2>

            <div class="informacoes">
                <div class="icone telefones">
                    <p>{{ $contato->telefone }}</p>
                    @foreach($contato->telefonesSecundariosArray() as $telefone_secundario)
                    <p>{{ $telefone_secundario }}</p>
                    @endforeach
                </div>

                <div class="icone email">
                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                </div>

                <div class="icone endereco">
                    {!! $contato->endereco !!}
                </div>
            </div>

            <form action="{{ route('contato.envio') }}" method="POST">
                    {{ csrf_field() }}
                    @if(session('success'))
                    <div class="response success">
                        Mensagem enviada com sucesso!
                    </div>
                    @elseif (count($errors) > 0)
                    <div class="response error">
                        @foreach($errors->all() as $error)
                        <p>{!! $error !!}</p>
                        @endforeach
                    </div>
                    @endif

                    <input type="text" name="nome" id="nome" placeholder="Nome" value="{{ old('nome') }}" @if($errors->has('nome'))class="error"@endif>
                    <input type="email" name="email" id="email" placeholder="E-mail" value="{{ old('email') }}" @if($errors->has('email'))class="error"@endif>
                    <input type="text" name="telefone" id="telefone" placeholder="Telefone" value="{{ old('telefone') }}" @if($errors->has('telefone'))class="error"@endif>
                    <input type="text" name="empresa" id="empresa" placeholder="Empresa" value="{{ old('empresa') }}" @if($errors->has('empresa'))class="error"@endif>
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem" @if($errors->has('mensagem'))class="error"@endif>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="ENVIAR">
                </form>
        </div>
    </div>

    <div class="googlemaps">
        {!! $contato->googlemaps !!}
    </div>

@endsection
