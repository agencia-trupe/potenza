@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Serviços /</small> {{ $paginas[$pagina] }}</h2>
    </legend>

    {!! Form::model($servicos, [
        'route'  => ['painel.servicos.update', $servicos->id],
        'method' => 'patch'])
    !!}

    @include('painel.servicos.' . $pagina, ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
