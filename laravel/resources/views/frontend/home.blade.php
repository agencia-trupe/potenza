@extends('frontend.common.template')

@section('content')

    <div class="home-banners">
        <a href="{{ route('servicos') }}" class="banner" style="background-image: url('{{ asset('assets/img/layout/bg-home-1.jpg') }}')">
            <div class="center">
                <div class="titulo">
                    <span style="font-size:128px;" data-desktop-size="128">Limpeza</span>
                    <span style="font-size:72px;" data-desktop-size="72">& Conservação</span>
                </div>
                <div class="subtitulo">
                    <span style="font-size:22px" data-desktop-size="22">Utilizamos equipamentos que potencializam a produção de nossos colaboradores.</span>
                </div>
                <div class="chamada">
                    <span style="font-size:28px" data-desktop-size="28">Conheça nossos serviços</span>
                </div>
                <img src="{{ asset('assets/img/layout/banner2.png') }}" alt="">
            </div>
        </a>
        <a href="{{ route('servicos') }}" class="banner" style="background-image: url('{{ asset('assets/img/layout/bg-home-2.jpg') }}')">
            <div class="center">
                <div class="titulo">
                    <span style="font-size:116px;" data-desktop-size="116">Trabalho</span>
                    <span style="font-size:89px;" data-desktop-size="89">Temporário</span>
                </div>
                <div class="subtitulo">
                    <span style="font-size:23px" data-desktop-size="23">Para as necessidades de condomínios, empresas, escolas e construção civil.</span>
                </div>
                <div class="chamada">
                    <span style="font-size:28px" data-desktop-size="28">Conheça nossos serviços</span>
                </div>
                <img src="{{ asset('assets/img/layout/banner1.png') }}" alt="">
            </div>
        </a>
        <a href="{{ route('servicos') }}" class="banner" style="background-image: url('{{ asset('assets/img/layout/bg-home-3.jpg') }}')">
            <div class="center">
                <div class="titulo">
                    <span style="font-size:85px;" data-desktop-size="85">Contratação</span>
                    <span style="font-size:152px;" data-desktop-size="152">Rápida</span>
                </div>
                <div class="subtitulo">
                    <span style="font-size:28px" data-desktop-size="28">Para o aumento de produção.</span>
                </div>
                <div class="chamada">
                    <span style="font-size:28px" data-desktop-size="28">Conheça nossos serviços</span>
                </div>
                <img src="{{ asset('assets/img/layout/banner3.png') }}" alt="">
            </div>
        </a>
    </div>

@endsection
