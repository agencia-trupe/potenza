<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servicos;

class ServicosController extends Controller
{
    private $paginas;

    public function __construct()
    {
        $this->paginas = Servicos::$paginas;
    }

    public function index()
    {
        $paginas = $this->paginas;

        return view('painel.servicos.index', compact('paginas'));
    }

    public function edit(Servicos $servicos, Request $request)
    {
        $paginas = $this->paginas;

        $pagina  = $request->query('pagina');
        $pagina  = (array_key_exists($pagina, $paginas) ? $pagina : 'home');

        return view('painel.servicos.edit', compact('paginas', 'servicos', 'pagina'));
    }

    public function update(ServicosRequest $request, Servicos $servicos)
    {
        try {

            $input = $request->all();

            $servicos->update($input);
            return redirect()->route('painel.servicos.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
