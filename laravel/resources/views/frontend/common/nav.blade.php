<li class="{{ Tools::isActive('quem-somos') }}">
    <a href="{{ route('quem-somos') }}">Quem Somos</a>
</li>
<li class="has-sub {{ Tools::isActive('servicos') }}">
    <a href="{{ route('servicos') }}">
        Serviços
        <span class="sub-mobile-handle"></span>
    </a>
    <ul class="sub-nav">
        @foreach($paginas as $slug => $pagina)
        <li class="{{ Tools::isActiveSlug($slug) }}">
            <a href="{{ route('servicos', $slug) }}">{{ $pagina }}</a>
        </li>
        @endforeach
    </ul>
</li>
<li class="{{ Tools::isActive('clientes') }}">
    <a href="{{ route('clientes') }}">Clientes</a>
</li>
<li class="{{ Tools::isActive('contato') }}">
    <a href="{{ route('contato') }}">Contato</a>
</li>
