<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QuemSomosRequest;
use App\Http\Controllers\Controller;

use App\Models\QuemSomos;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quemsomos = QuemSomos::first();

        return view('painel.quemsomos.index', compact('quemsomos'));
    }

    public function update(QuemSomosRequest $request, QuemSomos $quemsomos)
    {
        try {

            $input = $request->all();

            $quemsomos->update($input);
            return redirect()->route('painel.quem-somos.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
