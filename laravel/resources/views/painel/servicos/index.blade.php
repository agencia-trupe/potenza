@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Serviços
        </h2>
    </legend>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Página</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @foreach($paginas as $slug => $titulo)
            <tr>
                <td>{{ $titulo }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.servicos.edit', [1, 'pagina' => $slug]) }}" class="btn btn-primary btn-sm pull-left"><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection
