@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('terceirizacao_texto', 'Texto') !!}
    {!! Form::textarea('terceirizacao_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('terceirizacao_limpeza', 'Limpeza e Conservação Profissional') !!}
    {!! Form::textarea('terceirizacao_limpeza', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

<div class="form-group">
    {!! Form::label('terceirizacao_portaria', 'Portaria, Controle de Acesso e Recepção') !!}
    {!! Form::textarea('terceirizacao_portaria', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

<div class="form-group">
    {!! Form::label('terceirizacao_sazonalidade', 'Sazonalidade') !!}
    {!! Form::textarea('terceirizacao_sazonalidade', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
