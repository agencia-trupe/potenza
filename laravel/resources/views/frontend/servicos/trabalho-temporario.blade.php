@extends('frontend.common.template')

@section('content')

    <div class="main servicos servicos-{{ $slug }}">
        <div class="center">
            <h2>Serviços</h2>
            <h3>{{ $paginas[$slug] }}</h3>

            <div class="texto-1">{!! $servicos->temporario_texto1 !!}</div>
            <div class="texto-2">
                <div class="wrapper">{!! $servicos->temporario_texto2 !!}</div>
            </div>
        </div>
    </div>

@endsection