    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ config('site.name') }}</a></h1>
            <ul id="nav-desktop">
                @include('frontend.common.nav')
            </ul>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <div class="telefone">
                <div>
                    <span>{{ $contato->telefone }}</span>
                </div>
            </div>
        </div>
        <ul id="nav-mobile">
            @include('frontend.common.nav')
        </ul>
    </header>
