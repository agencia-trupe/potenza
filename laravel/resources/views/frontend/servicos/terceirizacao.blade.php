@extends('frontend.common.template')

@section('content')

    <div class="main servicos servicos-{{ $slug }}">
        <div class="center">
            <h2>Serviços</h2>
            <h3>{{ $paginas[$slug] }}</h3>

            {!! $servicos->terceirizacao_texto !!}

            <div class="terceirizacao-cycle">
                <div class="slide" data-cycle-hash="limpeza">
                    <h4>Limpeza e Conservação Profissional</h4>
                    <div class="texto">{!! $servicos->terceirizacao_limpeza !!}</div>
                </div>
                <div class="slide" data-cycle-hash="portaria">
                    <h4>Portaria, Controle de Acesso e Recepção</h4>
                    <div class="texto">{!! $servicos->terceirizacao_portaria !!}</div>
                </div>
                <div class="slide" data-cycle-hash="sazonalidade">
                    <h4>Sazonalidade</h4>
                    <div class="texto">{!! $servicos->terceirizacao_sazonalidade !!}</div>
                </div>
                <a href="#" class="cycle-arrow cycle-prev">prev</a>
                <a href="#" class="cycle-arrow cycle-next">next</a>
            </div>
        </div>
    </div>

@endsection