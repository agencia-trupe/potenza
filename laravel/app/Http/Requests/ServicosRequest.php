<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'home_texto1'                => 'sometimes|required',
            'home_texto2'                => 'sometimes|required',
            'home_destaque'              => 'sometimes|required',
            'temporario_texto1'          => 'sometimes|required',
            'temporario_texto2'          => 'sometimes|required',
            'terceirizacao_texto'        => 'sometimes|required',
            'terceirizacao_limpeza'      => 'sometimes|required',
            'terceirizacao_portaria'     => 'sometimes|required',
            'terceirizacao_sazonalidade' => 'sometimes|required',
            'treinamento_texto'          => 'sometimes|required',
            'recrutamento_texto1'        => 'sometimes|required',
            'recrutamento_texto2'        => 'sometimes|required',
            'recrutamento_etapas'        => 'sometimes|required'
        ];
    }
}
