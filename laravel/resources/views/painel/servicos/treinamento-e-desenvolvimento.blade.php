@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('treinamento_texto', 'Texto') !!}
    {!! Form::textarea('treinamento_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
