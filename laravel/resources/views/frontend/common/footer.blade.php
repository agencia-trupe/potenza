    <footer>
        <div class="center">
            <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            <p>
                © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                <a href="http://trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
