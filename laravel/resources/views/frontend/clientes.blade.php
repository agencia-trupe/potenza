@extends('frontend.common.template')

@section('content')

    <div class="main clientes">
        <div class="center">
            <h2>Clientes</h2>

            <div class="clientes-thumbs">
            @foreach($clientes as $cliente)
                <div class="cliente">
                    <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{ $cliente->nome }}" title="{{ $cliente->nome }}">
                </div>
            @endforeach
            </div>
        </div>
    </div>

@endsection
