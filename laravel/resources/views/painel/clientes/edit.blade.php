@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Cliente</h2>
    </legend>

    {!! Form::model($cliente, [
        'route'  => ['painel.clientes.update', $cliente->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.clientes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
