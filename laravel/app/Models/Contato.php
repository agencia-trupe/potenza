<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';

    protected $guarded = ['id'];

    public function telefonesSecundariosArray()
    {
        return array_map('trim', explode(',', $this->telefones_secundarios));
    }
}
