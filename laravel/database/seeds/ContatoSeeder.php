<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone'              => '11 4787 8100',
            'telefones_secundarios' => '11 4786 2415, 11 7000 2033',
            'email'                 => 'comercial@potenzarh.com.br',
            'endereco'              => '<p><strong>POTENZA - Empresa de Trabalho Tempor&aacute;rio</strong></p><p>Rua Santa Luzia, 603 - Vila Santa Luzia</p><p>06754-005 - Tabo&atilde;o da Serra, SP</p>',
            'googlemaps'            => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.9453004706993!2d-46.7544434!3d-23.6062946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5423b2eb9daf%3A0x1ee0be6d20bcda2b!2sR.+Santa+Luzia%2C+603+-+Vila+Santa+Luzia%2C+Tabo%C3%A3o+da+Serra+-+SP%2C+06754-005!5e0!3m2!1spt-BR!2sbr!4v1441804280037" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
        ]);
    }
}
