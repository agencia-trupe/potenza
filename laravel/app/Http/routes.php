<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('quem-somos', ['as' => 'quem-somos', 'uses' => 'QuemSomosController@index']);
Route::get('servicos/{slug?}', ['as' => 'servicos', 'uses' => 'ServicosController@index']);
Route::get('clientes', ['as' => 'clientes', 'uses' => 'ClientesController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('quem-somos', 'QuemSomosController');
    Route::resource('servicos', 'ServicosController');
    Route::resource('clientes', 'ClientesController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
