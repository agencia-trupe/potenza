<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicos')->insert([
            'home_texto1'                => 'home_texto1',
            'home_texto2'                => 'home_texto2',
            'home_destaque'              => 'home_destaque',
            'temporario_texto1'          => 'temporario_texto1',
            'temporario_texto2'          => 'temporario_texto2',
            'terceirizacao_texto'        => 'terceirizacao_texto',
            'terceirizacao_limpeza'      => 'terceirizacao_limpeza',
            'terceirizacao_portaria'     => 'terceirizacao_portaria',
            'terceirizacao_sazonalidade' => 'terceirizacao_sazonalidade',
            'treinamento_texto'          => 'treinamento_texto',
            'recrutamento_texto1'        => 'recrutamento_texto1',
            'recrutamento_texto2'        => 'recrutamento_texto2',
            'recrutamento_etapas'        => 'recrutamento_etapas'
        ]);
    }
}
