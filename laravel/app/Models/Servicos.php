<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public static $paginas = [
        'nossos-servicos'               => 'Nossos Serviços',
        'trabalho-temporario'           => 'Trabalho Temporário e Seleção',
        'terceirizacao'                 => 'Terceirização • Limpeza, Manutenção, Portaria e Recepção',
        'treinamento-e-desenvolvimento' => 'Treinamento e Desenvolvimento de Profissionais',
        'recrutamento-e-selecao'        => 'Recrutamento e Seleção'
    ];
}
