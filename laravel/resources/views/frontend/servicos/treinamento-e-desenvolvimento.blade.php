@extends('frontend.common.template')

@section('content')

    <div class="main servicos servicos-{{ $slug }}">
        <div class="center">
            <h2>Serviços</h2>
            <h3>{{ $paginas[$slug] }}</h3>

            <div class="texto-1">{!! $servicos->treinamento_texto !!}</div>
            <div class="imagem">
                <img src="{{ asset('assets/img/layout/clientes-atendidos.png') }}" alt="">
            </div>
        </div>
    </div>

@endsection