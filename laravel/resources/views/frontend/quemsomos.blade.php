@extends('frontend.common.template')

@section('content')

    <div class="main quemsomos">
        <div class="center">
            <h2>Quem Somos</h2>

            <div class="row">
                <div class="left">
                    <div class="texto">{!! $quemsomos->texto !!}</div>
                    <div class="destaque"><p>{!! $quemsomos->destaque !!}</p></div>
                </div>

                <div class="right">
                    <img src="{{ asset('assets/img/layout/mapa.png') }}" alt="" class="mapa">
                </div>
            </div>

            <div class="row">
                <div class="left">
                    <h3>Evolução da Receita (R$ - Milhões)</h3>
                    <img src="{{ asset('assets/img/layout/infografico-barras.png') }}" alt="">
                </div>

                <div class="right quemsomos-clientes">
                    <h3>Alguns dos nossos clientes</h3>
                    <div>
                        <img src="{{ asset('assets/img/layout/quem-somos_bauducco.png') }}" alt="">
                        <img src="{{ asset('assets/img/layout/quem-somos_gertec.png') }}" alt="">
                        <img src="{{ asset('assets/img/layout/quem-somos_pdg.png') }}" alt="">
                        <img src="{{ asset('assets/img/layout/quem-somos_sao-paulo-turismo.png') }}" alt="">
                        <img src="{{ asset('assets/img/layout/quem-somos_davo.png') }}" alt="">
                        <img src="{{ asset('assets/img/layout/quem-somos_daslu.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
