@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('temporario_texto1', 'Texto 1') !!}
    {!! Form::textarea('temporario_texto1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

<div class="form-group">
    {!! Form::label('temporario_texto2', 'Texto 2') !!}
    {!! Form::textarea('temporario_texto2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao-bullets']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
