<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('home_texto1');
            $table->text('home_texto2');
            $table->text('home_destaque');
            $table->text('temporario_texto1');
            $table->text('temporario_texto2');
            $table->text('terceirizacao_texto');
            $table->text('terceirizacao_limpeza');
            $table->text('terceirizacao_portaria');
            $table->text('terceirizacao_sazonalidade');
            $table->text('treinamento_texto');
            $table->text('recrutamento_texto1');
            $table->text('recrutamento_texto2');
            $table->text('recrutamento_etapas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicos');
    }
}
