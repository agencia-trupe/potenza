@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Quem Somos</h2>
    </legend>

    {!! Form::model($quemsomos, [
        'route'  => ['painel.quem-somos.update', $quemsomos->id],
        'method' => 'patch'])
    !!}

    @include('painel.quemsomos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
