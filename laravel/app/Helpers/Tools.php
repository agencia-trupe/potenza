<?php

namespace App\Helpers;

class Tools
{
    public static function isActive($routeName)
    {
        $currentRoute = \Route::currentRouteName() ?: null;
        return str_is($routeName, $currentRoute) ? 'active' : '';
    }

    public static function isActiveSlug($slug)
    {
        $currentRoute = \Route::currentRouteName() ?: null;
        $current = \Route::current() ? \Route::current()->parameter('slug') : null;
        $current = str_is('servicos', \Route::currentRouteName()) && $current === null ? 'nossos-servicos' : $current;

        return $slug == $current ? 'active' : '';
    }
}
