@extends('frontend.common.template')

@section('content')

    <div class="main servicos servicos-{{ $slug }}">
        <div class="center">
            <h2>Serviços</h2>
            <h3>{{ $paginas[$slug] }}</h3>

            <div class="row-1">
                <div class="left">
                    {!! $servicos->home_texto1 !!}
                    <h4>A Potenza atua de forma precisa nos seguintes negócios:</h4>
                    <img src="{{ asset('assets/img/layout/infografico-circulo.png') }}" alt="">
                </div>

                <div class="right">
                    <ul>
                        <li class="nossos-servicos-1">
                            <span>Temporários para acréscimo de serviçoes e substituição de quadro efetivo;</span>
                        </li>
                        <li class="nossos-servicos-2">
                            <span>Recrutamento e seleção de profissionais qualificados;</span>
                        </li>
                        <li class="nossos-servicos-3">
                            <span>Motoristas e manobristas;</span>
                        </li>
                        <li class="nossos-servicos-4">
                            <span>Recepção;</span>
                        </li>
                        <li class="nossos-servicos-5">
                            <span>Limpeza profissional;</span>
                        </li>
                        <li class="nossos-servicos-6">
                            <span>Controle de acesso;</span>
                        </li>
                        <li class="nossos-servicos-7">
                            <span>Controle, operacionalização e fiscalização de portarias;</span>
                        </li>
                        <li class="nossos-servicos-8">
                            <span>Terceirização de RH;</span>
                        </li>
                        <li class="nossos-servicos-9">
                            <span>Mensageiros e contínuos.</span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="destaque">
                <span>{{ $servicos->home_destaque }}</span>
            </div>

            <div class="row-2">
                <div class="left">{!! $servicos->home_texto2 !!}</div>
                <div class="right">
                    <img src="{{ asset('assets/img/layout/foto.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection