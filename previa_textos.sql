SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `clientes` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `contato` (
`id` int(10) unsigned NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones_secundarios` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `contato` (`id`, `telefone`, `telefones_secundarios`, `email`, `endereco`, `googlemaps`, `created_at`, `updated_at`) VALUES
(1, '11 4787 8100', '11 4786 2415, 11 7000 2033', 'comercial@potenzarh.com.br', '<p><strong>POTENZA - Empresa de Trabalho Tempor&aacute;rio</strong></p><p>Rua Santa Luzia, 603 - Vila Santa Luzia</p><p>06754-005 - Tabo&atilde;o da Serra, SP</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.9453004706993!2d-46.7544434!3d-23.6062946!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5423b2eb9daf%3A0x1ee0be6d20bcda2b!2sR.+Santa+Luzia%2C+603+-+Vila+Santa+Luzia%2C+Tabo%C3%A3o+da+Serra+-+SP%2C+06754-005!5e0!3m2!1spt-BR!2sbr!4v1441804280037" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_09_09_124640_create_contato_table', 1),
('2015_09_09_124649_create_contatos_recebidos_table', 1),
('2015_09_09_124657_create_clientes_table', 1),
('2015_09_09_124943_create_servicos_table', 1),
('2015_09_09_124950_create_quem_somos_table', 1);

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `quem_somos` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `destaque` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `quem_somos` (`id`, `texto`, `destaque`, `created_at`, `updated_at`) VALUES
(1, '<p>Presente num segmento de grande concorr&ecirc;ncia, a Potenza tem se destacado ao longo de 7 anos de atua&ccedil;&atilde;o, sedimentando sua hist&oacute;ria a cada novo contrato, ano a ano, &nbsp;cliente a cliente, at&eacute; se capacitar &nbsp;para &nbsp;os &nbsp;grandes contratos que hoje administra.</p>\r\n\r\n<p>A empresa conta com quadro societ&aacute;rio com experi&ecirc;ncia de mais de 16 anos em administra&ccedil;&atilde;o de RH e terceiriza&ccedil;&atilde;o de servi&ccedil;os e m&atilde;o de obra.</p>\r\n\r\n<p>A constru&ccedil;&atilde;o de um nome que tem o respeito de seus colaboradores, fornecedores e clientes sobre uma base eficiente, com foco na comunica&ccedil;&atilde;o e supervis&atilde;o, que faz toda a diferen&ccedil;a.</p>\r\n', 'Estamos presentes em todas as capitais brasileiras através de filiais e parcerias locais.', '0000-00-00 00:00:00', '2015-09-15 16:45:02');

CREATE TABLE IF NOT EXISTS `servicos` (
`id` int(10) unsigned NOT NULL,
  `home_texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `home_texto2` text COLLATE utf8_unicode_ci NOT NULL,
  `home_destaque` text COLLATE utf8_unicode_ci NOT NULL,
  `temporario_texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `temporario_texto2` text COLLATE utf8_unicode_ci NOT NULL,
  `terceirizacao_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `terceirizacao_limpeza` text COLLATE utf8_unicode_ci NOT NULL,
  `terceirizacao_portaria` text COLLATE utf8_unicode_ci NOT NULL,
  `terceirizacao_sazonalidade` text COLLATE utf8_unicode_ci NOT NULL,
  `treinamento_texto` text COLLATE utf8_unicode_ci NOT NULL,
  `recrutamento_texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `recrutamento_texto2` text COLLATE utf8_unicode_ci NOT NULL,
  `recrutamento_etapas` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `servicos` (`id`, `home_texto1`, `home_texto2`, `home_destaque`, `temporario_texto1`, `temporario_texto2`, `terceirizacao_texto`, `terceirizacao_limpeza`, `terceirizacao_portaria`, `terceirizacao_sazonalidade`, `treinamento_texto`, `recrutamento_texto1`, `recrutamento_texto2`, `recrutamento_etapas`, `created_at`, `updated_at`) VALUES
(1, '<p>Atrav&eacute;s de uma an&aacute;lise da necessidade, a <strong>POTENZA</strong> disponibiliza um servi&ccedil;o especializado e customizado de acordo com cada projeto, otimizando recursos para o cliente e proporcionando excelente ambiente de trabalho para o colaborador.</p>\r\n', '<p><strong>DOCUMENTA&Ccedil;&Atilde;O REGULAR</strong></p>\r\n\r\n<p>Ao contratar uma empresa de terceiriza&ccedil;&atilde;o e trabalho tempor&aacute;rio exija documenta&ccedil;&atilde;o regular:</p>\r\n\r\n<ul>\r\n	<li>Cadastro Nacional da Pessoa Jur&iacute;dica &ndash; C.N.P.J;</li>\r\n	<li>Contrato Social e Altera&ccedil;&otilde;es &ndash; Conhe&ccedil;a o s&oacute;cio administrador constantes no contrato Social;&nbsp;</li>\r\n	<li>Alvar&aacute; de Funcionamento;</li>\r\n	<li>Certid&atilde;o Municipal de Tributos Mobili&aacute;rios- ISS;&nbsp;</li>\r\n	<li>Certid&atilde;o Municipal de Tributos Imobili&aacute;rios &ndash; IPTU;</li>\r\n	<li>Certid&atilde;o Negativa de D&eacute;bitos Tribut&aacute;rios da Divida Ativa do Estado de S&atilde;o Paulo;&nbsp;</li>\r\n	<li>Certid&atilde;o Negativa Estadual de D&eacute;bitos Fiscais Relativos ao ICM/ICMS;</li>\r\n	<li>Certid&atilde;o de Tributos Federais da D&iacute;vida Ativa da Uni&atilde;o- Receita Federal;&nbsp;</li>\r\n	<li>Certid&atilde;o de Contribui&ccedil;&otilde;es Previdenci&aacute;rias &ndash; INSS;</li>\r\n	<li>Certid&atilde;o de Regularidade do FGTS &ndash; CRF;</li>\r\n	<li>Certid&atilde;o F&oacute;rum Comarca de Tabo&atilde;o da Serra- Cart&oacute;rios;&nbsp;</li>\r\n	<li>Certid&atilde;o de Fal&ecirc;ncia e Concordata;</li>\r\n	<li>Certid&atilde;o Negativa de D&eacute;bitos Trabalhistas;</li>\r\n	<li>Certificado de Registro como Empresa de Trabalho Tempor&aacute;rio junto ao Minist&eacute;rio do Trabalho e Emprego.</li>\r\n</ul>\r\n', 'EXCELÊNCIA NA QUALIDADE DE PRESTAÇÃO DE SERVIÇO PARA AS MAIORES EMPRESAS DO PAÍS, EM DIVERSOS SETORES. SEMpRE COM A GARANTIA DE PREÇOS BAIXOS.', '<p>Sele&ccedil;&atilde;o e contrata&ccedil;&atilde;o de m&atilde;o de obra tempor&aacute;ria para acr&eacute;scimo de servi&ccedil;os e/ou substitui&ccedil;&atilde;o de quadro efetivo.</p>\r\n\r\n<p><strong>M&Atilde;O DE OBRA TEMPOR&Aacute;RIA- LEI 6019/ 74</strong></p>\r\n\r\n<p>Contrata&ccedil;&atilde;o r&aacute;pida para o aumento de produ&ccedil;&atilde;o.</p>\r\n\r\n<p>Muitas vezes o seu neg&oacute;cio precisa de uma for&ccedil;a de trabalho maior, que seja capaz de acompanhar a demanda do mercado em algumas datas espec&iacute;ficas.</p>\r\n\r\n<p>A Potenza coloca &agrave; sua disposi&ccedil;&atilde;o uma estrutura de Departamento Pessoal rigorosamente atenta &agrave;s leis trabalhistas e conven&ccedil;&otilde;es coletivas de cada sindicato.</p>\r\n\r\n<p><strong>Assim o cliente est&aacute; sempre tranquilo quanto &agrave;s quest&otilde;es TRABALHISTAS E PREVIDENCI&Aacute;RIAS. Atuamos ainda com foco na SUPERVIS&Atilde;O sempre dispon&iacute;vel para atender aos questionamentos de nossos colaboradores.</strong></p>\r\n', '<p><strong>VANTAGENS NOS SEGMENTOS EM QUE ATUAMOS</strong></p>\r\n\r\n<ul>\r\n	<li>Apresenta&ccedil;&atilde;o de certid&otilde;es negativas atualizadas;</li>\r\n	<li>Apresenta&ccedil;&atilde;o de guias de recolhimento: Previd&ecirc;ncia, FGTS, extratos;</li>\r\n	<li>Seguro de vida e conv&ecirc;nio m&eacute;dico aos funcion&aacute;rios, mantendo a motiva&ccedil;&atilde;o para manter-se no emprego e evitando trocas constante de pessoal;</li>\r\n	<li>Pagamentos salariais e de benef&iacute;cios, recolhimento de FGTS sempre em dia, mantendo o n&iacute;vel de satisfa&ccedil;&atilde;o de nossos colaboradores;</li>\r\n	<li>Assessoria em recrutamento, sele&ccedil;&atilde;o, treinamento e administra&ccedil;ao de pessoal, utilizando-nos de estrutura moderna e equipe experiente, atendendo as necessidades dos clientes com profissionais de todos os n&iacute;veis hier&aacute;rquicos.</li>\r\n</ul>\r\n', '<p>Para se concentrar na atividade fim da empresa muitas tarefas de apoio podem ser terceirizadas. A <strong>POTENZA</strong> auxilia na cria&ccedil;&atilde;o de projetos adequados a cada caso para suprir tais necessidades.</p>\r\n', '<p>A Potenza tem conquistado este mercado de grande concorr&ecirc;ncia por interm&eacute;dio de uma filosofia de trabalho que:</p>\r\n\r\n<ul>\r\n	<li>Prioriza a valoriza&ccedil;&atilde;o e a supervis&atilde;o de seu quadro de funcion&aacute;rios que se torna sua grande for&ccedil;a junto ao cliente, com baixo absente&iacute;smo;</li>\r\n	<li>Investe em equipamentos que potencializam a produ&ccedil;&atilde;o &nbsp;de seus colaboradores, trazendo resultados de qualidade, com menor esfor&ccedil;o;</li>\r\n	<li>Investe em treinamento sobre t&eacute;cnicas de limpeza adequadas;</li>\r\n	<li>Utiliza somente produtos de uso profissional, tornando-se parceira de importantes marcas que atendem ao segmento de limpeza profissional;</li>\r\n	<li>Substitui imediatamente os funcion&aacute;rios quando necess&aacute;rio.</li>\r\n</ul>\r\n', '<p>A Potenza sabe cuidar da porta de entrada de sua empresa, condom&iacute;nio e escola.</p>\r\n\r\n<p>A &nbsp;atua&ccedil;&atilde;o da Potenza no segmento de Seguran&ccedil;a come&ccedil;a com uma visita &agrave; &aacute;rea a ser atendida, com estudo do p&uacute;blico e din&acirc;mica organizacional, a fim de estabelecermos os par&acirc;metros para a montagem de um perfil de equipe adequada a cada cliente. A partir deste perfil, as estrat&eacute;gias s&atilde;o definidas evitando situa&ccedil;&otilde;es indesejadas.</p>\r\n\r\n<ul>\r\n	<li>M&atilde;o de obra treinada para seu tipo de ambiente profissional e p&uacute;blico;&nbsp;</li>\r\n	<li>Ve&iacute;culo e equipe de Apoio;&nbsp;</li>\r\n	<li>Uniformes Personalizados;</li>\r\n	<li>Acompanhamento constante dos profissionais;</li>\r\n	<li>Equipes com um L&iacute;der em contato constante com Supervisor Potenza;</li>\r\n	<li>R&aacute;pida substitui&ccedil;&atilde;o em caso de aus&ecirc;ncia ou insatisfa&ccedil;&atilde;o do cliente com o profissional.</li>\r\n</ul>\r\n', '<p>Contrata&ccedil;&atilde;o em qualquer &aacute;rea da empresa ou atendimentoa demandas extraordin&aacute;rias:</p>\r\n\r\n<ul>\r\n	<li>Picos sazonais/ projetos especiais</li>\r\n	<li>Substitui&ccedil;&otilde;es de pessoal efetivo em f&eacute;rias</li>\r\n	<li>Licen&ccedil;as/ Imprevistos</li>\r\n</ul>\r\n\r\n<p>Vantagens:</p>\r\n\r\n<ul>\r\n	<li>Flexibilidade</li>\r\n	<li>Otimiza&ccedil;&atilde;o de custos/ seguran&ccedil;a</li>\r\n	<li>Agilidade na contrata&ccedil;&atilde;o e recoloca&ccedil;&atilde;o de pessoal</li>\r\n	<li>Elimina&ccedil;&atilde;o de processos burocr&aacute;ticos</li>\r\n</ul>\r\n\r\n<p>Datas onde o mercado aquece e pede trabalho tempor&aacute;rio:</p>\r\n\r\n<ul>\r\n	<li>Carnaval</li>\r\n	<li>P&aacute;scoa</li>\r\n	<li>Dia das M&atilde;es</li>\r\n	<li>Dia dos Namorados</li>\r\n	<li>Dia dos Pais</li>\r\n	<li>Dia das Crian&ccedil;as</li>\r\n	<li>Natal</li>\r\n	<li>Reveill&oacute;n</li>\r\n</ul>\r\n', '<p>A <strong>POTENZA</strong> investe na capacita&ccedil;&atilde;o constante de seus profissionais para garantir a excel&ecirc;ncia em seus servi&ccedil;os prestados. Profissionais treinados s&atilde;o cada vez mais importantes nas organiza&ccedil;&otilde;es pois s&atilde;o uma vantagem competitiva num mercado cada vez mais acirrado.</p>\r\n\r\n<p>Mesmo focando em servi&ccedil;os de terceiriza&ccedil;&atilde;o que n&atilde;o s&atilde;o a principal atividade da empresa, a <strong>POTENZA</strong> garante treinamento efetivo que contribui para o desempenho do pr&oacute;prio funcion&aacute;rio. Uma prova disso &eacute; o alto &iacute;ndice de contrata&ccedil;&atilde;o de m&atilde;o de obra para o primeiro emprego que permanece no quadro de funcion&aacute;rios da <strong>POTENZA</strong>.</p>\r\n', '<p>A <strong>POTENZA</strong> conta com uma equipe especializada e altamente treinada para encontrar o profissional certo para o seu neg&oacute;cio. Com grande experi&ecirc;ncia no recrutamento e sele&ccedil;&atilde;o de pessoal a <strong>POTENZA</strong> desenvolveu uma metodologia eficaz para a recoloca&ccedil;&atilde;o de profissionais t&eacute;cnicos, operacionais e administrativos, que prov&ecirc; o perfil ideal no momento certo para a sua empresa.</p>\r\n', '<p><strong>RECRUTAMENTO</strong></p>\r\n\r\n<ul>\r\n	<li>Publica&ccedil;&atilde;o de an&uacute;ncios em m&iacute;dias especializadas;</li>\r\n	<li>Triagem de curr&iacute;culos na base de dados;</li>\r\n	<li>Capta&ccedil;&atilde;o atrav&eacute;s de ferramentas digitais, Entidades, R&aacute;dio e Eventos;</li>\r\n	<li>Busca de profissionais experientes para fun&ccedil;&otilde;es espec&iacute;ficas.</li>\r\n</ul>\r\n\r\n<p><strong>SELE&Ccedil;&Atilde;O</strong></p>\r\n\r\n<ul>\r\n	<li>Processo de Recrutamento e Sele&ccedil;&atilde;o das vagas;</li>\r\n	<li>Aplica&ccedil;&atilde;o de testes com equipe qualificada;&nbsp;</li>\r\n	<li>Apresenta&ccedil;&atilde;o de candidatos finalistas;</li>\r\n	<li>Acompanhamento e controle de vagas.</li>\r\n</ul>\r\n', '<p><strong>1.</strong> Visita ao cliente</p>\r\n\r\n<p><strong>2.</strong> Levantamento de perfil e necessidades</p>\r\n\r\n<p><strong>3.</strong> Recrutamento</p>\r\n\r\n<p><strong>4.</strong> Entrevista</p>\r\n\r\n<p><strong>5.</strong> Aplica&ccedil;&atilde;o de testes</p>\r\n\r\n<p><strong>6.</strong> Apresenta&ccedil;&atilde;o do candidato</p>\r\n\r\n<p><strong>7.</strong> Apoio na decis&atilde;o</p>\r\n', '0000-00-00 00:00:00', '2015-09-15 16:54:31');

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$w/yxQD2IaZoMi4cq6uX68.5XWZnDZrmq42P1ozypmThhuFErXad.K', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');


ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `contato`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `contatos_recebidos`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

ALTER TABLE `quem_somos`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `servicos`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);


ALTER TABLE `clientes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `quem_somos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `servicos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
